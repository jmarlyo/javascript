// Función anónima auto-invocada
// Patrón módulo, función sin nombre
const miJuego = (() => {
    // Decirle al navegador que sea estricto 
    // nuevo scope
    'use strict'
    let deck = [];
    const tipos = ['C', 'D', 'H', 'S'],
          especiales = ['A', 'J', 'Q', 'K'];

    // let puntosJugador = 0,
    //     puntosComputadora = 0;
    let puntosJugadores = [];

    // Referencias del HTML - Botones
    const btnPedir = document.querySelector('#btnPedir');
    const btnDetener = document.querySelector('#btnDetener');
    const btnNuevo = document.querySelector('#btnNuevo');
    // Referencias del HTML - Cartas
    const divCartasJugadores = document.querySelectorAll('.divCarta');
    // Referencias del HTML - Texto puntaje [0] jugador - [1] pc
    const puntosHTML = document.querySelectorAll('small');

    const inicializarJuego = (numJugadores = 2) => {
        deck = crearDeck();
        puntosJugadores = [];
        for (let i = 0; i < numJugadores; i++) {
            puntosJugadores.push(0);      
        }
        puntosHTML.forEach(elem => elem.innerHTML = 0);
        divCartasJugadores.forEach(elem => elem.innerHTML = '');

        btnPedir.disabled = false;
        btnDetener.disabled = false;
    }
    // Esta función crea una nueva baraja de cartas
    const crearDeck = () => {
        deck = []
        for (let i = 2; i <= 10; i++) {
            for (let tipo of tipos) {
                deck.push(i + tipo);
            }
        }

        for (let tipo of tipos) {
            for (let esp of especiales) {
                deck.push(esp + tipo);
            }
        }
        // underscore libreria que con el método shuffle me revuelve o barajea las cartas
        return _.shuffle(deck);
    }
    // Esta función me permite tomar una carta
    const pedirCarta = () => {
        if (deck.length === 0) {
            throw 'No hay cartas en el deck'; // muestra error en consola 
        }
        return deck.pop();
    }

    // Función que me determina el valor de la carta que estamos pidiendo
    const valorCarta = (carta) => {
        //método subtring me corta el string como yo lo desee
        const valor = carta.substring(0, carta.length - 1);
        //función isNAN (is not a number) evalue lo que esta en el string no sea un número
        return (isNaN(valor)) ?
            (valor === 'A') ? 11 : 10
            : valor * 1;
    }
    const acumularPuntos = ( turno, carta ) => {
        puntosJugadores[turno] = puntosJugadores[turno] + valorCarta(carta);
        puntosHTML[turno].innerText = puntosJugadores[turno];
        return puntosJugadores[turno];
    }
    const crearCarta = (carta, turno) => {
        // <img class="carta" src="assets/cartas/2C.png">
        const imgCarta = document.createElement('img');
        imgCarta.src = `assets/cartas/${carta}.png`; //3H, JD
        imgCarta.classList.add('carta');
        divCartasJugadores[turno].append(imgCarta);
    }

    const quienGana = () => {
        //Ejecutar este callback en una cantidad de tiempo
        // Destructuración de arreglos
        const [ puntosMinimos, puntosComputadora ] = puntosJugadores;
        setTimeout(() => {
            if (puntosComputadora === puntosMinimos) {
                alert('Nadie gana :(');
            } else if (puntosMinimos > 21) {
                alert('Computadora gana')
            } else if (puntosComputadora > 21) {
                alert('Jugador Gana');
            } else {
                alert('Computadora Gana')
            }
        }, 100);
    }
    // turno de la computadora
    const turnoComputadora = (puntosMinimos) => {
        let puntosComputadora = 0;
        do {
            const carta = pedirCarta();
            puntosComputadora = acumularPuntos(puntosJugadores.length-1, carta);
            crearCarta(carta, puntosJugadores.length-1);
        } while ((puntosComputadora < puntosMinimos) && (puntosMinimos <= 21)); 
        quienGana();
    }

    // Eventos - agregar evento click
    // Callback, función que se esta enviando como argumento
    btnPedir.addEventListener('click', () => {

        const carta = pedirCarta();
        // Se muestra la suma de los puntos en el label small
        const puntosJugador = acumularPuntos(0, carta);
        crearCarta(carta, 0);

        if (puntosJugador > 21) {
            console.warn('Lo siento mucho, perdiste');
            btnPedir.disabled = true;
            btnDetener.disabled = true;
            turnoComputadora(puntosJugador);

        } else if (puntosJugador === 21) {
            console.warn('21, genial!');
            btnPedir.disabled = true;
            btnDetener.disabled = true;
            turnoComputadora(puntosJugador);
        }

    });

    btnDetener.addEventListener('click', () => {
        btnPedir.disabled = true;
        btnDetener.disabled = true;
        turnoComputadora(puntosJugadores[0]);
    });

    btnNuevo.addEventListener('click', () => {
        inicializarJuego();
    });
    // Esto va a ser público 
    return { nuevoJuego: inicializarJuego };
})();

