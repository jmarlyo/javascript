import "./styles.css";
import { buscarHeroe, buscarHeroeAsync } from "./js/promesas";
import { obtenerHeroes, obtenerHeroeAwait } from "./js/await";
// import { promesaLenta, promesaMedia, promesaRapida } from "./js/promesas.js";

// Promise.race([promesaRapida, promesaMedia, promesaLenta])
//     .then(mensaje => {
//         console.log(mensaje);
//     });

// Ejemplo de uso de async

// buscarHeroe('capi')
//     .then(console.log)
//     .catch( err => console.error(err));

// buscarHeroeAsync('ironman')
//     .then(console.log)
//     .catch( err => console.error(err));

// Ejemplo de uso de Await

// obtenerHeroes().then(console.table);

// Manejo de errores con el await
obtenerHeroeAwait('capi2').then(console.log);


