const heroes = {
    capi: {
        nombre: 'Capitán América',
        poder: 'Aguantar inyecciones sin morir'
    },
    iron: {
        nombre: 'Ironman',
        poder: 'Absurda cantidad de dinero'
    },
    spider: {
        nombre: 'Spiderman',
        poder: 'La mejor reacciona alergica a las picaduras de arañas'
    },
}


export const buscarHeroe = ( id ) => {
    
    const heroe = heroes[id];

    //dos argumentos, uno cuando se resuelve la promesa
    // y el otro si se rechaza
    return new Promise( ( resolve, reject ) => {
        if( heroe ) {
            // resolve (se envian los argumentos que quiero retornar)
            setTimeout(() => resolve( heroe ), 1000);

        } else {
            reject(`No existe un héroe con el id ${ id }`);
        }
    });
}

// Async me retorna por defecto una nueva promesa 

export const buscarHeroeAsync = async( id ) => {
    
    const heroe = heroes[id];

    if( heroe ) {
        return heroe; // esto es el mismo resolve de mi promesa
    } else {
        throw `No existe un héroe con el id ${ id }`; // este es el reject de mi promesa, cuando se el error
        throw Error ( `Hubo un error inesperado`); // este es el reject de mi promesa, cuando no conozco el error
    }
}


const promesaLenta = new Promise( ( resolve, reject ) => {
    setTimeout(() => resolve('Promesa Lenta') , 2000);
});

const promesaMedia = new Promise( ( resolve, reject ) => {
    setTimeout(() => resolve('Promesa Media') , 1500);
});

const promesaRapida = new Promise( ( resolve, reject ) => {
    setTimeout(() => resolve('Promesa Rápida') , 1000);
});


export {
    promesaLenta,
    promesaMedia,
    promesaRapida
}
